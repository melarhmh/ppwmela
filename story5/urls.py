from django.urls import path
from . import views

app_name ="story5"

urlpatterns = [
    path('form', views.buat_jadwal, name="schedule"),
    path('listform', views.daftar_jadwal, name="list_schedule"),
    path('deleteschedule', views.hapus_jadwal, name='hapus_jadwal'),
]