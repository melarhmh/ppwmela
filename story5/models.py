from django.db import models

# Create your models here.
class Schedule(models.Model):
	date = models.DateTimeField()
	place = models.CharField(max_length=100)
	event = models.CharField(max_length=100)
	category = models.CharField(max_length=30)