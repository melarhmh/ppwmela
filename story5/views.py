from django.shortcuts import render
from django.shortcuts import render, redirect
from .forms import ScheduleForm
from .models import Schedule
# Create your views here.

def daftar_jadwal(request):
    schedules = Schedule.objects.all().values()
    return render(request, 'JadwalMela.html', {'schedules': schedules})

def buat_jadwal(request):
    if request.method == "POST":
        form = ScheduleForm(request.POST)
        if form.is_valid():
            new_sch = form.save()
            new_sch.save()
            return redirect('story5:list_schedule')
    else:
        form = ScheduleForm()
    return render(request,'DaftarJadwal.html', {'form': form})

def hapus_jadwal(request):
	schedule = Schedule.objects.all().delete()
	return render(request, 'JadwalMela.html', {'schedules': schedule})