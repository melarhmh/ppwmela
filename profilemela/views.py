from django.shortcuts import render
from django.http import HttpResponse

def homepage(request):
	return render(request, "story_3.html", {})
def portfolio(request):
	return render(request, "story_3_pg2.html", {})
def signup(request):
	return render(request, "challenge_3.html", {})

# Create your views here.
