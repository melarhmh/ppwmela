from django.urls import path
from . import views

urlpatterns = [
	path('', views.homepage, name="home"),
	path('', views.portfolio, name="portfolio"),
	path('', views.signup, name="signup")
]